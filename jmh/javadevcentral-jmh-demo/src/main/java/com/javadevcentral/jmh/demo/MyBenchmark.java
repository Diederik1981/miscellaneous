/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.javadevcentral.jmh.demo;

import org.openjdk.jmh.annotations.*;

import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public class MyBenchmark {

    private static String input = "avbnfknkngqognqrubvxvibiobaaaigqbngibrgbqirgbiqngovfxofgporignqignqirgniqvoignsihaaaartopsnhoznohnoahinkisknbknbiosnbinpibnpinbpianfnqbhfbbvtyvtfvjbjfbquobfoinrglksngmgnqbgoiabfbrqginnseggeozwbvvfkfpogurehnqkldjfnrlvdkznjudfkenfivyuendcbvjglfpdqrpingqibgaaavbnfknkngqognqrubvxvibiobaaaigqbngibrgbqirgbiqngovfxofgporignqignqirgniqvoignsihaaaartopsnhoznohnoahinkisknbknbiosnbinpibnpinbpianfnqbhfbbvtyvtfvjbjfbquobfoinrglksngmgnqbgoiabfbrqginnseggeozwbvvfkfpogurehnqkldjfnrlvdkznjudfkenfivyuendcbvjglfpdqrpingqibgaaavbnfknkngqognqrubvxvibiobaaaigqbngibrgbqirgbiqngovfxofgporignqignqirgniqvoignsihaaaartopsnhoznohnoahinkisknbknbiosnbinpibnpinbpianfnqbhfbbvtyvtfvjbjfbquobfoinrglksngmgnqbgoiabfbrqginnseggeozwbvvfkfpogurehnqkldjfnrlvdkznjudfkenfivyuendcbvjglfpdqrpingqibgaaavbnfknkngqognqrubvxvibiobaaaigqbngibrgbqirgbiqngovfxofgporignqignqirgniqvoignsihaaaartopsnhoznohnoahinkisknbknbiosnbinpibnpinbpianfnqbhfbbvtyvtfvjbjfbquobfoinrglksngmgnqbgoiabfbrqginnseggeozwbvvfkfpogurehnqkldjfnrlvdkznjudfkenfivyuendcbvjglfpdqrpingqibgaaavbnfknkngqognqrubvxvibiobaaaigqbngibrgbqirgbiqngovfxofgporignqignqirgniqvoignsihaaaartopsnhoznohnoahinkisknbknbiosnbinpibnpinbpianfnqbhfbbvtyvtfvjbjfbquobfoinrglksngmgnqbgoiabfbrqginnseggeozwbvvfkfpogurehnqkldjfnrlvdkznjudfkenfivyuendcbvjglfpdqrpingqibgaaavbnfknkngqognqrubvxvibiobaaaigqbngibrgbqirgbiqngovfxofgporignqignqirgniqvoignsihaaaartopsnhoznohnoahinkisknbknbiosnbinpibnpinbpianfnqbhfbbvtyvtfvjbjfbquobfoinrglksngmgnqbgoiabfbrqginnseggeozwbvvfkfpogurehnqkldjfnrlvdkznjudfkenfivyuendcbvjglfpdqrpingqibgaaavbnfknkngqognqrubvxvibiobaaaigqbngibrgbqirgbiqngovfxofgporignqignqirgniqvoignsihaaaartopsnhoznohnoahinkisknbknbiosnbinpibnpinbpianfnqbhfbbvtyvtfvjbjfbquobfoinrglksngmgnqbgoiabfbrqginnseggeozwbvvfkfpogurehnqkldjfnrlvdkznjudfkenfivyuendcbvjglfpdqrpingqibgaa";

    @Benchmark
    @Fork(value = 2)
    @Measurement(iterations = 10, time = 1)
    @Warmup(iterations = 5, time = 1)
    public String stringDiederik() {
        char[] count = new char[26];
        for (int x = 0; x < input.length(); x++) {
            count[input.charAt(x) - 'a']++;
        }
        List<Pair> pairs = new ArrayList<>();
        for (int x = 0; x < count.length; x++) {
            if (count[x] != 0) {
                pairs.add(new Pair(count[x], (char) (x + 'a')));
            }
        }
        return pairs.stream()
                .sorted((a, b) -> Integer.compare(b.count, a.count))
                .map(p -> p.value)
                .collect(Collector.of(
                        StringBuilder::new,
                        StringBuilder::append,
                        StringBuilder::append,
                        StringBuilder::toString));
    }

    @Benchmark
    @Fork(value = 2)
    @Measurement(iterations = 10, time = 1)
    @Warmup(iterations = 5, time = 1)
    public String stringRainbow() {
        String msg = input;
        class DuplicatedLetter {
            final int character;
            final int frequency;
            DuplicatedLetter(int character) {
                this.character = character;
                this.frequency = (int) msg.chars().filter(c -> c == character).count();
            }
            int frequency() {
                return frequency;
            }
            CharBuffer write(CharBuffer buffer) {
                buffer.append((char) character);
                return buffer;
            }
        }

        CharBuffer buffer = CharBuffer.allocate(msg.length());
        return msg.chars()
                .distinct()
                .mapToObj(DuplicatedLetter::new)
                .sorted(Comparator.comparingInt(DuplicatedLetter::frequency).reversed())
                .<CharBufferConsumer>map(letter -> letter::write)
                .reduce(CharBufferConsumer::andThen)
                .orElseThrow()
                .consumeAndConvert(buffer);
    }
    @Benchmark
    @Fork(value = 2)
    @Measurement(iterations = 10, time = 1)
    @Warmup(iterations = 5, time = 1)
    public String stringMyl() {
        return input.chars().distinct().mapToObj(e -> String.format("%d%c", input.chars().filter(ch -> ch == e).count(), e)).sorted(Collections.reverseOrder()).map(e -> e.substring(1)).collect(Collectors.joining(""));
    }


    @FunctionalInterface
    interface CharBufferConsumer {
        CharBuffer accept(CharBuffer charBuffer);
        default String consumeAndConvert(CharBuffer charBuffer) {
            return new String(accept(charBuffer).array());
        }
        default CharBufferConsumer andThen(CharBufferConsumer consumer) {
            return cb -> consumer.accept(accept(cb));
        }
    }

    private static class Pair {
        char value;
        int count;
        Pair(int c, char v) {
            this.value = v;
            this.count = c;
        }
    }
}