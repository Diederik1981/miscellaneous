package nl.ddeboer.mazeserver.server;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.ServerWebSocket;
import lombok.Data;
import nl.ddeboer.mazeserver.game.map.MapRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MainServer extends AbstractVerticle {

    @Value("${server.refreshRateMS}")
    int refreshRateMS;

    @Value("${server.websocket.port}")
    int port;

    MapRepository mr = new MapRepository();


    List<ServerWebSocket> connections = new ArrayList<>();

    int currentMap = 1;

    @Override
    public void start() throws Exception {
        HttpServer httpserver = vertx.createHttpServer();

        httpserver.webSocketHandler(client -> {
            System.out.println("init :" + client);
            connections.add(client);

            client.textMessageHandler(event -> {
                System.out.println("handle text : " + event);
            });
            client.closeHandler(c -> {
                System.out.println("// TODO : close");
            });

        }).listen(port);

        vertx.setPeriodic(refreshRateMS, event -> {
            String message = getMessage();
            connections.forEach(c -> {c.writeTextMessage(message);});
        });
    }

    private String getMessage() {
        currentMap = 7; //++;
        if (currentMap == mr.getMapCount()) {
            currentMap = 1;
        }
        ObjectMapper om = new ObjectMapper();
        MyMap map = new MyMap();
        map.data = mr.getMap(currentMap);


        try {
            return om.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Data
    private static class MyMap {

        private String command = "CMD_LOAD_MAP";

        private List<String> data;

    }
}
