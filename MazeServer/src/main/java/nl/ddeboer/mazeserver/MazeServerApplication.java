package nl.ddeboer.mazeserver;

import io.vertx.core.Vertx;
import nl.ddeboer.mazeserver.server.MainServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class MazeServerApplication {

    @Autowired
    private MainServer mainServer;

    public static void main(String[] args) {
        SpringApplication.run(MazeServerApplication.class, args);
    }


    @PostConstruct
    public void deployVerticle() throws Exception {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(mainServer);
    }
}
