package nl.ddeboer.mazeserver.game.exception;

import nl.ddeboer.mazeserver.core.CoreException;

public class GameException extends CoreException {

	public GameException(String message) {
		super(message);
	}
}
