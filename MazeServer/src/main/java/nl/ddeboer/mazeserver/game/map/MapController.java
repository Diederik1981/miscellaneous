//package nl.ddeboer.mazeserver.game.map;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@RequestMapping(value = "/maps")
//@RestController
//public class MapController {
//
//	@Autowired
//	private MapService mapService;
//
//	@GetMapping("/{id}")
//	public Object getMap(@PathVariable int id) {
//		return mapService.getMap(id);
//
//	}
//
////		♈ Aries
////		♉ Taurus
////		♊ Gemini
////		♋ Cancer
////		♌ Leo
////		♍ Virgo
////		♎ Libra
////		♏ Scorpio
////		♐ Sagittarius
////		♑ Capricorn
////		♒
////		♓
////		⛎
//
////		char a = '❎';
////		char b = '⬜';
////		char c = '🔴';
//
////		char[][] map = new char[][] {
////			{'⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛',},
////			{'⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛',},
////			{'⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛',},
////			{'⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛',},
////			{'⬛','⭕','⬜','⬜','⬜','⬜','⬜','⬜','⬜','❎','⬛',},
////			{'⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛',},
////			{'⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛',},
////			{'⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛','⬛',}
////		};
////
////		return MapResponse.of(map);
////	}
////
//
//}
