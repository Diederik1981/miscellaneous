package nl.ddeboer.mazeserver.game.map.exception;


import nl.ddeboer.mazeserver.core.CoreException;

public class MapException extends CoreException {

	public MapException(String message) {
		super(message);

	}

}
