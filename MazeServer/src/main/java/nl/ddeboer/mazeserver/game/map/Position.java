package nl.ddeboer.mazeserver.game.map;

import lombok.Data;

@Data
public class Position {

	final int row;
	final int col;

	private Position(int r, int c) {
		row = r;
		col = c;
	}

	public static Position of(int r, int c) {
		return new Position(r, c);
	}

	public Position plus(Position p) {
		return Position.of(row + p.row, col + p.col);
	}

}
