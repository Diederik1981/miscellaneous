package nl.ddeboer.mazeserver.game;//package socketstuff.game;
//
//import javax.validation.Valid;
//import javax.validation.constraints.Max;
//import javax.validation.constraints.Min;
//import javax.validation.constraints.NotNull;
//
//import lombok.Data;
//import nl.ddeboer.core.CoreRequest;
//import nl.ddeboer.game.GameService.Command;
//
//@Data
//public class GameActionRequest extends @Valid CoreRequest {
//
//	@NotNull
//	@Min(value = 1)
//	@Max(value = 10000)
//	private Integer level;
//
//	@NotNull(message = "Must be one of: [START,EAST,WEST,NORTH,SOUTH]")
//	private Command command;
//
//}
