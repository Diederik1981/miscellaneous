package nl.ddeboer.mazeserver.game.map;

import lombok.extern.log4j.Log4j2;
import nl.ddeboer.mazeserver.game.map.exception.MapException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.file.Files.lines;

@Log4j2
public class MapRepository {

    private static final List<String> level1 = Collections.unmodifiableList(Arrays.asList(
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛",
            "⬛⭕⬜⬜⬜⬜⬜⬜⬜❎⬛",
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛"
    ));
    private static final List<String> level2 = Collections.unmodifiableList(Arrays.asList(
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛",
            "⬛❎⬜⬜⬜⬜⬜⬜⬜⭕⬛",
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛"
    ));
    private static final List<String> level3 = Collections.unmodifiableList(Arrays.asList(
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬛⬛⬛⬛⬛⬜⬜⬜❎⬛",
            "⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛",
            "⬛⭕⬜⬜⬜⬜⬜⬛⬛⬛⬛",
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛"
    ));
    private static final List<String> level4 = Collections.unmodifiableList(Arrays.asList(
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬜⬜⬜⬜⬛⬜⬜⬜⬜⬜⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬜⬛⬛⬜⬛⬜⬛⬛⬛⬜⬛⬜⬜⬜⬛⬛⬛",
            "⬛⬜⬛⬛⬜⬛⬜⬛⬛⬛⬜⬛⬜⬛⬜⬜❎⬛",
            "⬛⬜⬛⬛⬜⬛⬜⬛⬛⬛⬜⬛⬜⬛⬛⬛⬛⬛",
            "⬛⬜⬛⬛⬜⬛⬜⬛⬛⬛⬜⬜⬜⬛⬛⬛⬛⬛",
            "⬛⭕⬛⬛⬜⬜⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛"
    ));
    private static final List<String> level5 = Collections.unmodifiableList(Arrays.asList(
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬜⬛⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬜⬜⬜⬛⬛⬛⬜⬛⬛⬜⬛⬛⬛⬜⬜⬜⬜⬜⬛⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬜⬛⬜⬛⬛⬛⬜⬜⬜⬜⬛⬛⬛⬜⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬜⬛⬜⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬜⬛⬜⬛⬛⬛⬜⬛⬛⬜⬛⬛⬛⬜⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬜⬜⬜⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬜⬛⬜⬛⬛⬛⬜⬛⬛⬜⬛⬛⬛⬜⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬜⬛⬜⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬜⬛⬜⬛⬛⬛⬜⬛⬛⬜⬛⬛⬛⬜⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬜⬛⬜⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬜⬛⬜⬛⬛⬛⬜⬛⬛⬜⬛⬛⬛⬜⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬜⬛⬜⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬜⬛⬜⬜⬜⬜⬜⬛⬛⬜⬜⬜⬜⬜⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬜⬛⬜⬜⬜⬜⬜⬜❎⬛",
            "⬛⬜⬛⬜⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬜⬜⬜⬜⬜⬜⬜⬜⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬜⬛⬜⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛",
            "⬛⭕⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛"
    ));
    private static final List<String> level6 = Collections.unmodifiableList(Arrays.asList(
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛❎⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬛⬛",
            "⬛⭕⬜⬜⬜⬜⬜⬜⬜⬜⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜⬜⬛",
            "⬛⬛⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛⬛⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬛⬛⬜⬛",
            "⬛      ⬛⬜⬛   ⬛⬜⬛⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛⬛⬜⬛",
            "⬛⬛⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬜⬛⬛⬜⬛⬛⬜⬛",
            "⬛⬛⬛⬜⬜⬜⬜⬜⬜⬛⬛⬛⬜⬜⬜⬜⬜⬛⬛⬜⬛⬛⬜⬛⬜⬜⬜⬜⬜⬛",
            "⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛⬜⬛⬛⬜⬛⬛⬜⬛⬛⬜⬛",
            "⬛⬛⬛⬜⬜⬜⬜⬜⬜⬜⬛⬛⬛⬛⬜⬛⬛⬛⬛⬜⬛⬛⬜⬛⬛⬛⬛⬛⬜⬛",
            "⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛⬜⬜⬜⬜⬜⬜⬜⬜⬛⬜⬛",
            "⬛⬛⬛⬛⬛⬛⬜⬛     ⬛⬜⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬜⬛",
            "⬛⬛⬛⬛⬛⬛⬜⬛     ⬛⬜⬛   ⬛⬜⬜⬛    ⬛⬜⬛",
            "⬛⬛⬜⬜⬜⬜⬜⬛     ⬛⬜⬛   ⬛⬜⬛⬛   ⬛⬛⬜⬛",
            "⬛⬛⬛⬛⬛⬛⬜⬛     ⬛⬜⬛   ⬛⬜⬛    ⬛⬜⬜⬛",
            "⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛⬛⬜⬛    ⬛⬛⬜⬛",
            "⬛⬛⬛⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬛⬛⬜⬛     ⬛⬜⬛",
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛⬜⬛",
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜⬛⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜⬛",
            "⬛⬛⬛⬛⬛⬛⬜⬜⬜⬜⬜⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛",
            "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛"
    ));
    private static final List<String> level7 = Collections.unmodifiableList(Arrays.asList());


    /* @formatter:off */
    private static List<List<String>> maps = new ArrayList<>();
    private List<Position> startingPositions = new ArrayList<>(maps.size());
    private List<Position> targetPositions = new ArrayList<>(maps.size());

    {
        maps.add(null);
        maps.add(level1);
        maps.add(level2);
        maps.add(level3);
        maps.add(level4);
        maps.add(level5);
        maps.add(level6);
        try {
            maps.add(lines(Path.of("mapte500500")).collect(Collectors.toList()));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public MapRepository() {
        startingPositions.add(null);
        targetPositions.add(null);
        for (int x = 1; x < maps.size(); x++) {
            startingPositions.add(getStartingPositionForMap(x));
            targetPositions.add(getTargetPositionForMap(x));
        }

    }

    private static Position getPositionForCharInMap(char c, int index) {
        List<String> map = maps.get(index);
        for (int row = 0; row < map.size(); row++) {
            for (int col = 0; col < map.get(0).length(); col++) {
                if (map.get(row).charAt(col) == c) {
                    return Position.of(row, col);
                }
            }
        }
        throw new MapException("Could not find startposition for '" + c + "'in map: '" + index + "'");
    }

    private static Position getStartingPositionForMap(int index) {
        return getPositionForCharInMap('⭕', index);
    }

    private static Position getTargetPositionForMap(int index) {
        return getPositionForCharInMap('❎', index);
    }

//    public void makeTransparent(int index) {
//        List<String> strings = maps.get(index);
//
//        for (int row = 1; row < strings.size() - 1; row++) {
//            for (int col = 1; col < strings.get(row).length() - 1; col++) {
//
//                if (maps.get(index).get(row).charAt())
//
//
//            }
//
//        }
//
//    }

    public int getMapCount() {
        return maps.size();
    }

    public Position getStartPositionForMaze(int index) {
        return startingPositions.get(index);
    }

    public Position getTargetPositionForMaze(int index) {
        return targetPositions.get(index);
    }

    public List<String> getMap(int nr) {
        if (nr < 1 || nr >= maps.size()) {
            log.info("No map with nr: '" + nr + "' available");
            throw new MapException("No map with nr: '" + nr + "' available");
        }

        return maps.get(nr);
    }


}
