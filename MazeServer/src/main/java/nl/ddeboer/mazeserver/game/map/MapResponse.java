package nl.ddeboer.mazeserver.game.map;

public class MapResponse {

	private final char[][] data;

	private MapResponse(char[][] map) {
		this.data = map;
	}

	public static MapResponse of(char[][] map) {
		return new MapResponse(map);
	}
}
