package nl.ddeboer.mazeserver.game.map;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class MapService {
//	⬛❎⬜⭕
	public enum TileType {
		WALKABLE('⬜'), WALL('⬛'), START('⭕'), EXIT('❎');

		private final char c;

		TileType(char c) {
			this.c = c;
		}
	}

	HashMap<Character, TileType> charToTyleType = new HashMap<>();
	{
		Arrays.stream(TileType.values()).forEach(tt -> charToTyleType.put(tt.c, tt));
	}

	private MapRepository mapRepository = new MapRepository();

	public Map getMap(Integer index) {
		return Map.of(mapRepository.getMap(index));
	}

	public Position getStartPositionForMap(int index) {
		return mapRepository.getStartPositionForMaze(index);
	}

	public TileType checkLocation(int index, Position vector) {
		List<String> map = mapRepository.getMap(index);
		String line = map.get(vector.getRow());
		char c = line.charAt(vector.getCol());
		System.out.println(line + " " + c);
		return charToTyleType.get(c);
	}

	public Map getView(int index, int r, int c) {
		List<String> map = mapRepository.getMap(index);
		List<String> resp = new ArrayList<>();
		for (int row = r - 1; row <= r + 1; row++) {
			StringBuilder sb = new StringBuilder();
			for (int col = c - 1; col <= c + 1; col++) {
				sb.append(map.get(row).charAt(col));
			}
			resp.add(sb.toString());
		}
		return Map.of(resp);
	}

}
