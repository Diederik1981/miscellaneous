//package nl.ddeboer.mazeserver.game;
//
//import lombok.extern.log4j.Log4j2;
//import nl.ddeboer.game.exception.GameException;
//import nl.ddeboer.game.map.Map;
//import nl.ddeboer.game.map.MapService;
//import nl.ddeboer.game.map.MapService.TileType;
//import nl.ddeboer.game.map.Position;
//import nl.ddeboer.user.User;
//import nl.ddeboer.user.UserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import static nl.ddeboer.game.map.Position.of;
//
//@Service
//@Log4j2
//public class GameService {
//
//	public enum Command {
//		START(of(0, 0)), NORTH(of(1, 0)), EAST(of(0, 1)), SOUTH(of(-1, 0)), WEST(of(0, -1));
//
//		private final Position vector;
//
//		Command(Position vector) {
//			this.vector = vector;
//		}
//
//		public Position getVector() {
//			return vector;
//		}
//	}
//
//	@Autowired
//	private UserService userService;
//
//	@Autowired
//	private MapService mapService;
//
//	public Map startGame(GameActionRequest request) {
//		User user = userService.verifyAndGetUser(request.getUser());
//
//		if (request.getLevel() > user.getLevelsUnlocked()) {
//			log.info("user: '" + user.getUserName() + "' tried to access level: '" + request.getLevel() + "' while user max is: '" + user.getLevelsUnlocked());
//			throw new GameException("You can pick a level up to '" + user.getLevelsUnlocked() + "'");
//		}
//
//		user.setCurrentLevel(request.getLevel());
//
//		Position startPosition = mapService.getStartPositionForMap(request.getLevel());
//		user.setPos(startPosition);
//		log.info("starting level: '" + request.getLevel() + "' for player: '" + request.getUser().getUserName() + "'");
//		return getView(user);
//
//	}
//
//	public Map move(GameActionRequest request) {
//		User user = userService.verifyAndGetUser(request.getUser());
//		if (!user.getCurrentLevel().equals(request.getLevel())) {
//			throw new GameException("Level '" + request.getLevel() + "' is not your active level: '" + user.getCurrentLevel() + "'.");
//		}
//
//		Position tmp = user.getPos().plus(request.getCommand().getVector());
//
//		TileType checkLocation = mapService.checkLocation(request.getLevel(), tmp);
//		switch (checkLocation) {
//		case WALL:
//			System.out.println("wall");
//			return getView(user);
//		case WALKABLE:
//			System.out.println("wallkable");
//			user.setPos(tmp);
//			return getView(user);
//		case EXIT:
//			System.out.println("exit");
//			user.endlevel();
//			return mapService.getMap(request.getLevel());
//
//		default:
//			throw new GameException("case: '" + checkLocation + "' not implemented.");
//		}
//
//	}
//
//	private Map getView(User user) {
//		Map view = mapService.getView(user.getCurrentLevel(), user.getPos().getRow(), user.getPos().getCol());
//		return view;
//	}
//
//}
