//package nl.ddeboer.mazeserver.game;
//
//import lombok.extern.log4j.Log4j2;
//import nl.ddeboer.game.GameService.Command;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.*;
//
//@RequestMapping(value = "/mazegame")
//@RestController
//@Log4j2
//public class GameRestController {
//
//	@Autowired
//	private GameService gameService;
//
//	@GetMapping("/play")
//	private ResponseEntity<Object> startLevel(@RequestBody @Validated GameActionRequest request) {
//
//		if (request.getCommand() == Command.START) {
//			return ResponseEntity.ok(gameService.startGame(request));
//		} else {
//			return ResponseEntity.ok(gameService.move(request));
//		}
//
//	}
//
//	@PostMapping
//	private ResponseEntity<Object> move() {
//
//		return null;
//	}
//
//}
