package nl.ddeboer.mazeserver.game.map;

import lombok.Data;

import java.util.List;

@Data
public class Map {

	private List<String> data;

	public static Map of(List<String> list) {
		Map map = new Map();
		map.data = list;
		return map;
	}
}
