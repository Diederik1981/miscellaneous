package qtree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QueryGrid {

    private static int spacer = 50;
    private final List<CircleActor>[][] grid;

    public QueryGrid(int width, int height) {
        grid = new ArrayList[(width / spacer) + 1][(height / spacer) + 1];
        for (int x = 0; x < grid[0].length; x++) {
            for (int y = 0; y < grid.length; y++) {
                grid[y][x] = new ArrayList<>();
            }
        }
    }

    public boolean collides(CircleActor ca) {
        int x = (int) ca.getX() / spacer;
        int y = (int) ca.getY() / spacer;

        return grid[y][x].stream().anyMatch(s -> {
            if (s == ca) {
                return false;
            }
            return s.getBoundingRectangle().overlaps(ca.getBoundingRectangle());
        });
    }

    public void update() {
//        Arrays.stream(grid).flatMap(u -> Arrays.stream(u)).forEach(List::clear);
        for (int x = 0; x < grid[0].length; x++) {
            for (int y = 0; y < grid.length; y++) {
                grid[y][x].clear();
            }
        }
        for (CircleActor circleActor : Main.circleActors) {
            int x = (int) (circleActor.getX() / spacer);
            int y = (int) (circleActor.getY() / spacer);
            grid[y][x].add(circleActor);
        }
    }

}
