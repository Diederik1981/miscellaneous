package qtree;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import java.util.Set;

public class CircleActor extends Sprite {

    private final Vector2 velocity;
    public Set<CircleActor> location = null;

    public CircleActor(Texture t, Vector2 velocity, int x, int y) {
        super(t);
        this.velocity = velocity;
        this.setX(x);
        this.setY(y);
        this.setSize(5, 5);
        setColor(Color.GRAY);
    }

    public void update(float deltaTime) {
        translate(velocity.x * deltaTime, velocity.y * deltaTime);
        if (getX() + getWidth() > Main.WIDTH) {
            setX(0);
        }
        if (getX() < 0) {
            setX(Main.WIDTH - getWidth());
        }
        if (getY() + getHeight() > Main.HEIGHT) {
            setY(0);
        }
        if (getY() < 0) {
            setY(Main.HEIGHT - getHeight());
        }

        if (isColliding()) {
            setColor(Color.WHITE);
        } else {
            setColor(Color.GRAY);
        }

    }


    private boolean isColliding() {
        return Main.queryGrid.collides(this);
    }

}
