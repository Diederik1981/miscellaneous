package qtree;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * {@link com.badlogic.gdx.ApplicationListener} implementation shared by all platforms.
 */
public class Main extends ApplicationAdapter {

    public static int WIDTH = 1200;
    public static int HEIGHT = 1200;
    public static QueryGrid queryGrid;
    private static int totalCircleCount = 12000;
    public static List<CircleActor> circleActors = new ArrayList<>(totalCircleCount);
    Random r = new Random();
    private SpriteBatch batch;
    private Texture circleTexture;
    private FrameRate frameRate;

    @Override
    public void create() {
        batch = new SpriteBatch();
        circleTexture = new Texture("whiteCircleBig.png");
        frameRate = new FrameRate();

        queryGrid = new QueryGrid(WIDTH, HEIGHT);

        for (int x = 0; x < totalCircleCount; x++) {
            Vector2 velocity = new Vector2(r.nextFloat() * 20, r.nextFloat() * 20);
            circleActors.add(new CircleActor(circleTexture, velocity, r.nextInt(WIDTH), r.nextInt(HEIGHT)));
        }


    }


    @Override
    public void render() {
        float delta = Gdx.graphics.getDeltaTime();
        frameRate.update();
        circleActors.parallelStream().forEach(ca -> ca.update(delta));
        queryGrid.update();
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        frameRate.render();
        circleActors.forEach(ca -> ca.draw(batch));
        batch.end();

    }
}