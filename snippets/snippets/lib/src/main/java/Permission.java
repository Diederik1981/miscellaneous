import java.util.HashMap;
import java.util.Map;

public class Permission {

	private String parent;

	public Permission(String p) {
		parent = p;
	}

	public static void main(String[] args) {

		// input array:
		Permission[] perms = new Permission[3];
		perms[0] = new Permission("p1");
		perms[1] = new Permission("p2");
		perms[2] = new Permission("p3");

		// build your map with parent as key:
		Map<String, Permission> map = new HashMap<>();
		for (Permission p : perms) {
			map.put(p.getParent(), p);
		}
		
		
		// get permission by parent
		Permission permission = map.get("p1");
		
		// and print
		System.out.println(permission);

	}

	public String getParent() {
		return parent;
	}

	
	@Override
	public String toString() {
		return parent;
	}
}
