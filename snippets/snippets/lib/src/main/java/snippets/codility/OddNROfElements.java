package snippets.codility;

public class OddNROfElements {

    public static void main(String[] args) {

        System.out.println(solution(new int[]{1, 2, 3, 2, 1}));
    }


    static int solution(int ar[]) {
        int res = 0;
        for (int i = 0; i < ar.length; i++) {
            res = res ^ ar[i];
        }
        return res;
    }


}
