package snippets.codility;

import java.util.Arrays;
import java.util.TreeSet;
import java.util.Vector;

public class DNARank {

    public static void main(String[] args) {
//        System.out.println(Arrays.toString(solution("CAGCCTA", new int[]{2, 5, 0}, new int[]{4, 5, 6})));
//        System.out.println(Arrays.toString(solution("", new int[]{2, 5, 0}, new int[]{4, 5, 6})));
//        System.out.println(Arrays.toString(solution("A", new int[]{2, 5, 0}, new int[]{4, 5, 6})));



        Float f = Float.MAX_VALUE;
        f += 10000000;
        System.out.println(Float.MAX_VALUE);
        System.out.println(f);



    }

//    CAGCCTA


    public static int[] solution(String S, int[] P, int[] Q) {
        if (S.isEmpty()) {
            return new int[]{};
        }
        if (P.length == 0) {
            return P;
        }
        Dist[] dists = new Dist[S.length()];
        for (int x = 0; x < S.length(); x++) {
            dists[x] = new Dist();
        }

        int a = -1;
        int c = -1;
        int g = -1;

        for (int x = S.length() - 1; x >= 0; x--) {
            if (a > -1) a++;
            if (c > -1) c++;
            if (g > -1) g++;


            char tmp = S.charAt(x);
            if (tmp == 'A') a = 0;
            if (tmp == 'C') c = 0;
            if (tmp == 'G') g = 0;
            dists[x].a = a;
            dists[x].c = c;
            dists[x].g = g;

        }
        int[] result = new int[P.length];
        for (int x = 0; x < P.length; x++) {
            int start = P[x];
            int dist = Q[x] - start;
            System.out.println(dist + " " + start);
            if (dists[start].a >= 0 && dists[start].a <= dist) {
                result[x] = 1;
            } else if (dists[start].c >= 0 && dists[start].c <= dist) {
                result[x] = 2;
            } else if (dists[start].g >= 0 && dists[start].g <= dist) {
                result[x] = 3;
            } else {
                result[x] = 4;
            }
        }
        return result;
    }


    private static class Dist {
        int a;
        int c;
        int g;

        @Override
        public String toString() {
            return "Dist{" +
                    "a=" + a +
                    ", c=" + c +
                    ", g=" + g +
                    '}';
        }
    }


//    public static int[] solution(String S, int[] P, int[] Q) {
//        if (P.length == 0) {
//            return P;
//        }
//        if (S.length() == 0) {
//            return new int[P.length];
//        }
//
//        TreeSet<Integer> as = new TreeSet<>();
//        TreeSet<Integer> cs = new TreeSet<>();
//        TreeSet<Integer> gs = new TreeSet<>();
//        TreeSet<Integer> ts = new TreeSet<>();
//
//        int start = S.length() / 2;
//        int left = start;
//        int right = start + 1;
//        while (left >= 0 || right <= S.length()) {
//
//            switch (S.charAt(left)) {
//                case 'A':
//                    as.add(left);
//                    break;
//                case 'C':
//                    cs.add(left);
//                    break;
//                case 'G':
//                    gs.add(left);
//                    break;
//            }
//            left--;
//
//            if (right < S.length()) {
//
//                switch (S.charAt(right)) {
//                    case 'A':
//                        as.add(right);
//                        break;
//                    case 'C':
//                        cs.add(right);
//                        break;
//                    case 'G':
//                        gs.add(right);
//                        break;
//                }
//            }
//            right++;
//
//        }
//
//        int[] result = new int[Q.length];
//
//        for (int x = 0; x < P.length; x++) {
//            if (as.ceiling(P[x]) != null && as.ceiling(P[x]) <= Q[x]) {
//                result[x] = 1;
//            } else if (cs.ceiling(P[x]) != null && cs.ceiling(P[x]) <= Q[x]) {
//                result[x] = 2;
//            } else if (gs.ceiling(P[x]) != null && gs.ceiling(P[x]) <= Q[x]) {
//                result[x] = 3;
//            } else {
//                result[x] = 4;
//            }
//        }
//
//
//        return result;
//    }

}


