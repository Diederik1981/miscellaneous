package snippets.codility.stacksnqueues;

import java.util.Stack;

public class StoneWall {

    public static void main(String[] args) {

        System.out.println(solution(new int[]{8, 8, 5, 7, 9, 8, 7, 4, 8}));
//        System.out.println(solution(new int[]{8, 7, 7, 1, 4, 7, 2, 5, 8, 8}));

//        H[0] = 8    H[1] = 8    H[2] = 5
//        H[3] = 7    H[4] = 9    H[5] = 8
//        H[6] = 7    H[7] = 4    H[8] = 8

    }


    public static int solution(int[] arr) {


        Stack<Integer> pieces = new Stack<>();
        pieces.push(arr[0]);
        int count = 1;

        for (int x = 1; x < arr.length; x++) {

            int tmp = arr[x];
            int tmp2 = pieces.peek();

            if (tmp > tmp2) {
                pieces.push(tmp);
                count++;
            } else {
                while (!pieces.empty() && pieces.peek() > tmp) {
                    pieces.pop();
                }
                if (!pieces.isEmpty()) {
                    if (pieces.peek() != tmp) {
                        pieces.push(tmp);
                        count++;
                    }
                } else {
                    pieces.push(tmp);
                    count++;
                }
            }
        }


        return count;
    }


}
