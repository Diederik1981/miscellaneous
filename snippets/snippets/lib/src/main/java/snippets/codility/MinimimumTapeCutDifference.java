package snippets.codility;

public class MinimimumTapeCutDifference {

    public static void main(String[] args) {

        System.out.println(solution(new int[]{3, 1, 2, 4, 3}));

    }


    public static int solution(int[] a) {
        long leftcount = 0;
        for (int x = 0; x < a.length; x++) {
            leftcount += a[x];
        }
        long difference = Integer.MAX_VALUE;
        int rightcount = 0;
        for (int x = a.length - 1; x > 0; x--) {
            rightcount += a[x];
            leftcount -= a[x];
            long tmp = Math.abs(leftcount - rightcount);
            if (tmp < difference) {
                difference = tmp;
                if (difference == 0) {
                    break;
                }
            }
        }
        return (int) difference;
    }
}
