package snippets.codility;

public class FirstMissingIntegerFromList {

    public static void main(String[] args) {
        System.out.println(solution(new int[]{1, 2, 3, 4}));
        System.out.println(solution(new int[]{-1, 2, 3, 4}));
        System.out.println(solution(null));
        System.out.println(solution(new int[]{}));
    }


    public static int solution(int[] A) {
        int max = A.length;
        boolean[] hits = new boolean[max + 2];

        for (int x : A) {
            if (x > 0 && x <= max) {
                hits[x] = true;
            }
        }

        int missingNr = 1;
        while (hits[missingNr]) {
            missingNr++;
        }

        return missingNr;
    }

}
