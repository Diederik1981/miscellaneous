package snippets.codility.stacksnqueues;

import java.util.Stack;

public class Fish {

    public static void main(String[] args) {

//        System.out.println(solution());
        System.out.println(solution(new int[]{0, 1}, new int[]{1, 1}));
    }


    public static int solution(int[] A, int[] B) {

        Stack<Integer> stack = new Stack<>();
        int alive = 0;

        for (int x = 0; x < A.length; x++) {
            int dir = B[x];
            int str = A[x];
            if (dir == 0) {
                if (!stack.isEmpty()) {
                    while (!stack.isEmpty() && stack.peek() < str) {
                        stack.pop();
                    }
                    if (stack.isEmpty()) {
                        alive++;
                    }
                } else {
                    alive++;
                }
            } else {
                stack.push(str);
            }
        }
        return alive + stack.size();
    }
}
