package snippets.codility;

public class TriangleSort {

    public static void main(String[] args) {
        System.out.println(solution(new int[]{10, 2, 5, 1, 8, 20}));
        System.out.println(solution(new int[]{10, 50, 5, 1}));
    }


    public static int solution(int[] A) {
        for (int x = 0; x < A.length - 2; x++) {
            long tmp = A[x];
            if (tmp < 1) continue;
            for (int y = x + 1; y < A.length - 1; y++) {
                long tmp2 = A[y];
                if (tmp2 < 2) continue;
                for (int z = y + 1; z < A.length; z++) {
                    long tmp3 = A[z];
                    if (tmp3 > 2 && tmp3 >= Math.abs(tmp - tmp2) && tmp3 < tmp + tmp2) {
//                        if (tmp + tmp2 > tmp3 && tmp2 + tmp3 > tmp && tmp3 + tmp > tmp2) {
                            return 1;
//                        }
                    }
                }
            }
        }
        return 0;
    }

}
