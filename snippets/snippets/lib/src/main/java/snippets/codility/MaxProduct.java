package snippets.codility;

import java.util.Arrays;

public class MaxProduct {

    public static void main(String[] args) {
        System.out.println(solution(new int[]{1, 2, 3}));
        System.out.println(solution(new int[]{7, 1, 2, 3}));
        System.out.println("--");
        System.out.println(solution(new int[]{7, 1, 2, 3, 1, 1, -9, -9}));
    }


    public static int solution(int[] A) {
        if (A.length > 6) {

            Arrays.sort(A);
            int[] list = new int[6];
            int index = 0;
            while (index < 3) {
                list[index] = A[index];
                list[index + 3] = A[A.length - 1 - index];
                index++;
            }
            A = list;
        }


        int max = Integer.MIN_VALUE;
        for (int x = 0; x < A.length - 2; x++) {
            for (int y = x + 1; y < A.length - 1; y++) {
                for (int z = y + 1; z < A.length; z++) {
                    int tmp = A[x] * A[y] * A[z];
                    if (A[x] * A[y] * A[z] > max) {
                        max = tmp;
                    }

                }

            }
        }
        return max;
    }
}
