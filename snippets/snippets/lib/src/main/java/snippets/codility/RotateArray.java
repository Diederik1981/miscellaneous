package snippets.codility;

import org.checkerframework.checker.units.qual.A;
import org.checkerframework.checker.units.qual.K;

import java.util.Arrays;

public class RotateArray {

    public static void main(String[] args) {

        System.out.println(Arrays.toString(solution(null, 3)));
//        System.out.println(Arrays.toString(solution(new int[]{}, 3)));
//        System.out.println(Arrays.toString(solution(new int[]{0}, 3)));
//        System.out.println(Arrays.toString(solution(new int[]{0, 1}, 1)));
//        System.out.println(Arrays.toString(solution(new int[]{1, 2, 3}, 3)));
        System.out.println(Arrays.toString(solution(new int[]{1, 2, 3, 45, 6, 10}, 1)));
        System.out.println(Arrays.toString(solution(new int[]{1, 2, 3, 45, 6, 10}, 2)));
        System.out.println(Arrays.toString(solution(new int[]{1, 2, 3, 45, 6, 10}, 3)));
        System.out.println(Arrays.toString(solution(new int[]{1, 2, 3, 45, 6, 10}, 4)));
        System.out.println(Arrays.toString(solution(new int[]{1, 2, 3, 45, 6, 10}, 5)));
        System.out.println(Arrays.toString(solution(new int[]{1, 2, 3, 45, 6, 10}, 6)));


//        ([], 3)
        System.out.println("----");
        System.out.println(Arrays.toString(solution(new int[]{3, 8, 9, 7, 6}, 1)));
        System.out.println(Arrays.toString(solution(new int[]{3, 8, 9, 7, 6}, 3)));
    }

    public static int[] solution(int[] A, int K) {
        if (A == null || A.length <= 1 || K == 0 || K % A.length == 0) {
            return A;
        }
        K = A.length - (K % A.length);

        int[] result = new int[A.length];
        int pos = 0;

        for (int x = K; x < A.length; x++) {
            result[pos++] = A[x];
        }

        int pos2 = 0;
        for (int x = pos; x < A.length; x++) {
            result[x] = A[pos2++];
        }

        return result;
    }
}
