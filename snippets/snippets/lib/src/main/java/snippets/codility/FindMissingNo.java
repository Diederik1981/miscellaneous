package snippets.codility;

public class FindMissingNo {

    public static void main(String[] args) {

        System.out.println(getMissingNo(new int[]{4, 2, 2, 3, 5}));
        System.out.println(getMissingNo(new int[]{4, 2, 3, 5}));
        System.out.println(getMissingNo(new int[]{}));

    }

    static int getMissingNo(int a[]) {
        if (a == null || a.length == 0) {
            return 1;
        }

        int x1 = a[0];
        int x2 = 1;

        for (int i = 1; i < a.length; i++)
            x1 = x1 ^ a[i];

        for (int i = 2; i <= a.length + 1; i++)
            x2 = x2 ^ i;

        return (x1 ^ x2);
    }
}
