package snippets.codility;

public class DivisableByK {

    public static void main(String[] args) {
        System.out.println(solution(10, 20, 3));
    }


    public static int solution(int A, int B, int K) {
        int start = A - (A % K);
        if (start != A) {
            start += K;
        }
        if (start > B) {
            return 0;
        }
        int range = B - start;
        return (range / K) + 1;
    }
}
