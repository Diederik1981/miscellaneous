package snippets;
import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;

public class ColorWheel extends JPanel {
    private final JSlider Slider;

    public ColorWheel() {
        setLayout(new BorderLayout());
        Slider = new JSlider(SwingConstants.HORIZONTAL, 0, 360, 0);
        Slider.setMajorTickSpacing(30);
        Slider.setPaintTicks(true);
        Slider.setPaintLabels(true);

        add(Slider, BorderLayout.SOUTH);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
//        super.setBackground(Color.pink);
//        super.repaint();
//        g .setColor(Color.PINK);
        Graphics2D arc = (Graphics2D) g;
//        arc.setBackground(Color.PINK);

        arc.setColor(Color.GREEN);
        arc.fill(new Arc2D.Double(80, 30, 250, 250, 90, 90, Arc2D.PIE));
        arc.setColor(Color.BLUE);
        arc.fill(new Arc2D.Double(80, 30, 250, 250, 180, 90, Arc2D.PIE));
        arc.setColor(Color.YELLOW);
        arc.fill(new Arc2D.Double(80, 30, 250, 250, 270, 90, Arc2D.PIE));
        arc.setColor(Color.RED);
        arc.fill(new Arc2D.Double(80, 30, 250, 250, 0, 90, Arc2D.PIE));
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        JFrame frame = new JFrame();
        JPanel panel = new ColorWheel();
        frame.add(panel);
        panel.setBackground(Color.PINK);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 400);
        frame.setVisible(true);

    }

}