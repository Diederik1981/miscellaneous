package snippets.randomchallenge;

import java.util.Deque;
import java.util.LinkedList;
import java.util.stream.Collectors;

/**
 * Good morning! Here's your coding interview problem for today.
 *
 * This problem was asked by Quora.
 *
 * Given an absolute pathname that may have . or .. as part of it, return the
 * shortest standardized path.
 *
 * For example, given "/usr/bin/../bin/./scripts/../", return "/usr/bin/".
 */
public class Pathname {

    public static void main(String[] args) {
        System.out.println(shortPath("/usr/bin/../bin/./scripts/../"));
        System.out.println(shortPath("/../bin/../bin/./scripts/../"));
        System.out.println(shortPath("/a/b/../../../../c"));
        System.out.println(shortPath("/a/b/../c"));
    }


    public static String shortPath(String path) {

        String[] split = path.split("/");
        Deque<String> q = new LinkedList<>();

        for (String s : split) {
            if (s.equals(".")) {
                continue;
            }
            if (s.equals("..")) {
                if (!q.isEmpty()) {
                    q.removeLast();
                }
            } else {
                q.addLast(s);
            }
        }

        return q.stream().collect(Collectors.joining("/"));
    }



    public static String shortPath2(String path) {

        String[] split = path.split("/");

        for (int x = 1; x < split.length; x++) {
            String tmp = split[x].trim();
            if (tmp.equals(".")) {
                split[x] = null;
            } else if (tmp.equals("..")) {
                split[x] = null;
                int index = x - 1;
                while (index >= 0 && split[index] == null) {
                    index--;
                }
                split[index] = null;
            }
        }

        StringBuilder sb = new StringBuilder();
        for (String s : split) {
            if (s != null && !s.isEmpty()) {
                sb.append("/");
                sb.append(s);
            }
        }

        return sb.toString();
    }

}
