package snippets.randomchallenge;

import java.util.*;

public class Contains {

    public static void main(String[] args) {

        Set<Integer> original = new HashSet<>(Arrays.asList(1, 2, 3, 5));

        // incoming collection
        List<Integer> newset = new ArrayList<>(Arrays.asList(3, 5, 6, 7));

        Set<Integer> alreadyExists = new HashSet<>(newset);
        Set<Integer> didntExist = new HashSet<>(newset);

        alreadyExists.retainAll(original);
        didntExist.removeAll(original);

        original.addAll(didntExist);


        System.out.println("already in: " + alreadyExists);
        System.out.println("new elements: " + didntExist);


    }
}
