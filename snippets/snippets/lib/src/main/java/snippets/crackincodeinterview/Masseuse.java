package snippets.crackincodeinterview;

public class Masseuse {

    public static void main(String[] args) {
        System.out.println(schedule(new int[]{30, 15, 60, 75, 45, 15, 15, 45}));
    }


    public static int schedule(int[] arr) {
        System.out.println(schedule2(arr, 0));
        return schedule(arr, 0);
    }




    public static int schedule(int[] arr, int index) {
        return index >= arr.length - 1 ? 0 : Math.max(arr[index] + schedule(arr, index + 2), arr[index + 1] + schedule(arr, index + 3));
    }



    public static int schedule2(int[] arr, int index) {
        if (index >= arr.length - 1) {
            return 0;
        }

        if (arr[index + 1] < arr[index]) {
            return arr[index] + schedule(arr, index + 2);
        } else {
            return arr[index + 1] + schedule(arr, index + 3);
        }

    }
}
