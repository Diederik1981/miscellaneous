package snippets.lc;

import java.util.Arrays;

public class RotateMatrix90Percent {

    public static void main(String[] args) {

        int[][] matrix = new int[][]{
                {1, 2, 3}, {4, 5, 6}, {7, 8, 9}
        };


        for (int[] arr : matrix) {
            System.out.println(Arrays.toString(arr));
        }

        rotate(matrix);
        System.out.println("--");

        for (int[] arr : matrix) {
            System.out.println(Arrays.toString(arr));
        }
    }


    public static void rotate(int[][] arr) {

        int n = arr.length;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n - i; j++) {
                int temp = arr[i][j];
                arr[i][j] = arr[n - 1 - j][n - 1 - i];
                arr[n - 1 - j][n - 1 - i] = temp;
            }
        }

        for (int x = 0; x < arr.length / 2; x++) {
            int[] tmp = arr[x];
            arr[x] = arr[arr.length - 1 - x];
            arr[arr.length - 1 - x] = tmp;
        }

    }


}
