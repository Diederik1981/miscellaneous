//package snippets.lc;
//
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Map.Entry;
//import java.util.PriorityQueue;
//
//public class KMostElements {
//
//	public static void main(String[] args) {
//		System.out.println(Arrays.toString(topKFrequent(new int[] { 1, 1, 1, 2, 2, 3, 3, 44444, 44444, 44444 }, 3)));
//
//	}
//
//	public static int[] topKFrequent(int[] nums, int k) {
//
//		Map<Integer, Integer> map = new HashMap<>();
//		for (int i : nums) {
//			Integer count = map.getOrDefault(i, 0);
//			count++;
//			map.put(i, count);
//		}
//
//		PriorityQueue<Pair> pq = new PriorityQueue<>();
//		for (Entry<Integer, Integer> e : map.entrySet()) {
//			Pair p = new Pair(e.getKey(), e.getValue());
//			pq.add(p);
//		}
//		int[] result = new int[k];
//		for (int x = 0; x < k; x++) {
//			result[x] = pq.poll().key;
//		}
//		return result;
//	}
//
//	private static class Pair implements Comparable<Pair> {
//		int key;
//		int count;
//
//		public Pair(int k, int c) {
//			key = k;
//			count = c;
//		}
//
//		@Override
//		public int compareTo(Pair o) {
//
//			return o.count - count;
//		}
//	}
//}
