package snippets.lc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Sum3 {

    public static void main(String[] args) {
        System.out.println(threeSum(new int[]{1, 2}));
        System.out.println(threeSum(new int[]{1, 2, -3}));
        System.out.println(threeSum(new int[]{-1, 0, 1, 2, -1, -4}));
        System.out.println(List.of(1, 2).equals(Arrays.asList(1, 2)));

    }


    public static List<List<Integer>> threeSum(int[] nums) {
        if (nums == null || nums.length < 3) {
            return new ArrayList<>();
        }
        Arrays.sort(nums);
        List<List<Integer>> response = new ArrayList<>();
        for (int x = 0; x < nums.length - 2; x++) {
            int tmp1 = nums[x];
            if (tmp1 > 0) {
                x = nums.length;
                continue;
            }
            if (x > 0 && tmp1 == nums[x - 1]) {
                continue;
            }
            for (int y = x + 1; y < nums.length - 1; y++) {
                int tmp2 = nums[y];
                int xplusy = tmp1 + tmp2;
                if (xplusy >= 0) {
                    y = nums.length;
                    continue;
                }
                if (y > x + 1 && tmp2 == nums[y - 1]) {
                    continue;
                }
                for (int z = y + 1; z < nums.length; z++) {
                    int tmp3 = nums[z];
//                    if (tmp3 == tmp2) {
//                        continue;
//                    }
                    if (xplusy + tmp3 == 0) {
                        int index = response.size() - 1;
                        if (!response.isEmpty()) {
                            List<Integer> prev = response.get(response.size() - 1);
                            if (prev.get(0) == tmp1 && prev.get(1) == tmp2 && prev.get(2) == tmp3) {
                                continue;
                            }
                        }
                        response.add(List.of(tmp1, tmp2, tmp3));
                        z = nums.length;
                        continue;
                    }
                    if (tmp3 > 0 && xplusy + tmp3 > 0) {
                        z = nums.length;
                    }

                }
            }
        }


        return response;
    }


}
