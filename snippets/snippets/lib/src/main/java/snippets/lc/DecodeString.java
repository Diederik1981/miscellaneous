package snippets.lc;

public class DecodeString {

    public static void main(String[] args) {

//        System.out.println(decodeString("14[a]"));
//        System.out.println(decodeString("a2[z3[c1[ddd]]]1[x]"));
//        System.out.println(decodeString("14[a]2[bc]"));
//        System.out.println(decodeString("3[a2[c]]"));
//        System.out.println(decodeString("abc3[cd]xyz"));
        System.out.println(decodeString("3[z]2[2[y]pq4[2[jk]e1[f]]]ef"));
        System.out.println(decodeString("3[z]")); //2[2[y]pq4[2[jk]e1[f]]]ef"));

//         zzzyypqjkjkefefjkjkefefjkjkefefjkjkefefyypqjkjkefefjkjkefefjkjkefefjkjkefef
//           zzzyypqjkjkefjkjkefjkjkefjkjkefyypqjkjkefjkjkefjkjkefjkjkefef
//        "zzzyypqjkjkefjkjkefjkjkefjkjkefyypqjkjkefjkjkefjkjkefjkjkefef"


    }


    public static String decodeString(String s) {

        StringBuilder sb = new StringBuilder();

        decode(s, 0, sb);
        return sb.toString();
    }

    public static int decode(String str, int index, StringBuilder sb) {

        if (index >= str.length()) return str.length();

        while (index < str.length() && Character.isAlphabetic(str.charAt(index))) {
            sb.append(str.charAt(index));
            index++;
        }

        // calc digits
        int reps = 0;
        while (index < str.length() && Character.isDigit(str.charAt(index))) {
            reps *= 10;
            reps += (int) str.charAt(index) - '0';
            index++;
        }
        if (reps == 0) return index;
        index++;

        // do n times
        int index2 = 0;
        for (int x = 0; x < reps; x++) {
            index2 = decode(str, index, sb);
        }
        // see if there is more
        return decode(str, ++index2, sb);
    }

}
