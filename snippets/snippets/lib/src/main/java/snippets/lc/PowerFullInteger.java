package snippets.lc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PowerFullInteger {

//    public static void main(String[] args) {
//
//        System.out.println(powerfulIntegers(2, 3, 10));
//
//
//        System.out.println(funkyAddSumoid(100));
//
//
//        User user = null;
//        try {
//
//            findUser(20);
//        } catch (NormalUser e) {
//            user = e.getBody();
//        } catch (BuisinessUser e) {
//            user = e.getBody();
//        }
//
//    }
//
//
//    public static void findUser(int id) {
//
//        if (id < 100) {
//            throw new NormalUser();
//        }
//        throw new BuisinessUser();
//    }

    public static int funkyAddSumoid(int addzor) {
        return addzor == 0 ? 0 : addzor + funkyAddSumoid(addzor -1);
    }



    public static List<Integer> powerfulIntegers(int x, int y, int bound) {
        ArrayList<Integer> list1 = new ArrayList<>();


        int power = 0;
        while (true) {
            long tmp = (long) Math.pow(x, power);
            power++;
            if (tmp > bound) {
                break;
            }
            list1.add((int) tmp);
        }

        Set<Integer> sums = new HashSet<>();

        int pow = 0;
        while (true) {
            long tmp = (long) Math.pow(y, pow);
            pow++;
            if (tmp > bound) {
                break;
            }
            for (int i = 0; i < list1.size(); i++) {
                int tmp2 = list1.get(i);
                int sum = (int) (tmp2 + tmp);
                if (sum > bound) {
                    break;
                }
                sums.add(sum);

            }
        }
        return new ArrayList<>(sums);
    }
}
