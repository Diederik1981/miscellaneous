package snippets.lc;

public class NonDecreasingArray {

    public static void main(String[] args) {
        System.out.println(checkPossibility(new int[]{4, 2, 1}));
        System.out.println(checkPossibility(new int[]{3, 4, 2, 3}));
        System.out.println(checkPossibility(new int[]{4, 2, 3}));
    }

    public static boolean checkPossibility(int[] nums) {

        for (int x = 0; x < nums.length - 1; x++) {
            int tmp = nums[x];
            int tmp2 = nums[x + 1];
            if (tmp > tmp2) {
                return canPass(nums, x) || canPass(nums, x + 1);
            }
        }
        return true;
    }

    private static boolean canPass(int[] nums, int skip) {
        for (int x = 1; x < nums.length; x++) {

            if (x == skip) {
                continue;
            }
            int prev = nums[x - 1];
            if (skip == x - 1) {
                if (skip == 0) {
                    continue;
                }
                prev = nums[x - 2];
            }
            if (nums[x] < prev) {
                return false;
            }
        }
        return true;
    }

}
