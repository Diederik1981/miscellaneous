package snippets.lc;

public class LLPalin {

    public static void main(String[] args) {
        ListNode node = new ListNode(1);
        node.next = new ListNode(2);
        node.next.next = new ListNode(3);
        node.next.next.next = new ListNode(2);
        node.next.next.next.next = new ListNode(1);

//        System.out.println(node);
        System.out.println(isPalindrome(node));
    }


    public static boolean isPalindrome(ListNode head) {
        int count = 1;

        ListNode end = head;
        ListNode middle = end;
        while (end.next != null) {
            count++;
            if (count % 2 == 0) {
                middle = middle.next;
            }
            end = end.next;
        }
        System.out.println(middle);
        ListNode newStart = end;
        for (int x = 0; x < count / 2; x++) {
            end.next = head;

            head = head.next;
            end = end.next;
        }
        end.next = null;
//        System.out.println(head);

//        System.out.println(newStart);
        return false;
    }


    public static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public String toString() {
            return "ListNode{" +
                    "val=" + val +
                    ", next=" + next +
                    '}';
        }
    }
}
