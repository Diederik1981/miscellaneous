package mazeclient;

public class MazeResponse {

	private String[] data;

	public String[] getData() {
		return data;
	}

	public void setData(String[] data) {
		this.data = data;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (String s : data) {
			sb.append(s);
			sb.append("\n");
		}
		return sb.toString();
	}

}
