package mazeclient;

import java.io.IOException;
import java.util.ArrayList;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

public class TestKryoTest {

	public static void main(String[] args) throws IOException, Exception {

	    Client client = new Client();
	    
	    client.addListener(new Listener() {
	        public void received (Connection connection, Object object) {
	           if (object instanceof ArrayList) {
	        	   
					ArrayList<String> request = (ArrayList<String>) object;

					System.out.println("----TCP");
					System.out.println(connection.getRemoteAddressTCP());
					System.out.println("----UDP");
					System.out.println(connection.getRemoteAddressUDP());
					System.out.println("----");
					System.out.println(connection.getReturnTripTime());
					
					System.out.println(request);
					System.out.println("----RESPONSE---");

					ArrayList<String> response = new ArrayList<String>();
					response.add("thankts for the datadsflksdjfl;kdsjfsal;dkfjs;dlfjsadlkfjsa;lfjsdflkajsflsdjflksdjf;alskfjas;lfkjas;lfjasd;lfjas;lfkjasdfljsadlkfj");
					connection.sendTCP(response);
	           }
	        }
	     });

	    Kryo kryo = client.getKryo();
	    kryo.register(ArrayList.class);

	    client.start();
	    client.connect(5000, "localhost", 54555, 54777);
	    
	    ArrayList<String> request = new ArrayList<String>();
	    request.add("test for kyowyowammmm");
	    client.sendTCP(request);
	    
	    Thread.sleep(100000);
	    
	    
	    
		
	}

}
