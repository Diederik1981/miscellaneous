package mazeclient;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.Arrays;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Main {

	private static final HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_1_1)
			.connectTimeout(Duration.ofSeconds(3)).build();

	public static void main(String[] args) throws IOException, InterruptedException {

		printmap(3);
		Arrays.asList(null);

	}

	public static void printmap(int i) throws IOException, InterruptedException {
		HttpRequest request = HttpRequest.newBuilder().GET().uri(URI.create("http://localhost:8080/maps/" + i)).build();
		HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		ObjectMapper om = new ObjectMapper();
		System.out.println(om.readValue(response.body(), MazeResponse.class));
	}

}
