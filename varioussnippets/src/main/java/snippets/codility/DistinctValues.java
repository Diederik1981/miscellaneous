package snippets.codility;

import java.util.HashSet;
import java.util.Set;

public class DistinctValues {

    public static void main(String[] args) {
        System.out.println(solution(new int[]{1, 2, 3, 4}));
    }


    public static int solution(int[] A) {
        if (A == null) {
            return 0;
        }
        Set<Integer> ints = new HashSet<>();
        for (int x : A) {
            ints.add(x);
        }
        return ints.size();
    }
}
