package snippets.codility;

public class PermSequence {

    public static void main(String[] args) {
        System.out.println(solution(new int[]{4, 1, 3, 2}));
        System.out.println(solution(new int[]{1}));

    }


    static int solution(int a[]) {
        int x1 = a[0];
        int x2 = 1;

        for (int i = 1; i < a.length; i++)
            x1 = x1 ^ a[i];


        for (int i = 2; i <= a.length + 1; i++)
            x2 = x2 ^ i;

        if ((x1 ^ x2) == a.length + 1) {
            return 1;
        }
        return 0;
    }
}
