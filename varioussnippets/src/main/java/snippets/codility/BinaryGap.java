package snippets.codility;

public class BinaryGap {

    public static void main(String[] args) {

        for (int x = 0; x < 100; x ++) {
            System.out.println(solution(x));
        }

    }


    public static int solution(int N) {

        String s = Integer.toBinaryString(N);
        int index = s.lastIndexOf('1');
        if (index <= 0) {
            return 0;
        }
        s = s.substring(0, index + 1);

        int max = 0;
        int count = 0;
        for (int x = 0; x < s.length(); x++) {
            if (s.charAt(x) == '0') {
                count++;
            } else {
                max = Math.max(max, count);
                count = 0;

            }
        }
        return max;
    }
}
