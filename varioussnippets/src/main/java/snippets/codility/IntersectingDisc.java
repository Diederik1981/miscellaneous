package snippets.codility;//package snippets.codility;
//
//import java.util.ArrayList;
//import java.util.Collections;
//
//public class IntersectingDisc {
//
//    public static void main(String[] args) {
//        System.out.println(solution(new int[]{1, 5, 2, 1, 4, 0}));
//        System.out.println(solution(new int[]{12, 0, 1, 5, 2, 1, 4, 0}));
//    }
//
//
//    public static int solution(int[] A) {
//
//
//        ArrayList<Circle> list = new ArrayList<>();
//
//        for (int x = 0; x < A.length; x++) {
//            list.add(new Circle(x - A[x], x + A[x]));
//        }
//
//        Collections.sort(list, (c1, c2) -> Long.compare(c1.left, c2.left));
//
//        int overlaps = 0;
//        for (int x = 0; x < list.size() - 1; x++) {
//            Circle tmp = list.get(x);
//            for (int y = x + 1; y < list.size(); y++) {
//                if (list.get(y).left <= tmp.right) {
//                    overlaps++;
//                    if (overlaps > 10000000) {
//                        return -1;
//                    }
//                } else {
//                    y = list.size();
//                }
//            }
//
//        }
//
//        return overlaps;
//    }
//
//    private static class Circle {
//        long left;
//        long right;
//
//        Circle(int left, int right) {
//            this.left = left;
//            this.right = right;
//        }
//
//        @Override
//        public String toString() {
//            return "{left=" + left + ", right=" + right + "}, ";
//        }
//    }
//
//
////    int count = 0;
////        for (int x = 0; x < A.length - 1; x++) {
////        for (int y = x + 1; y < A.length; y++) {
////            if (x + A[x] >= y - A[y]) {
////                count++;
////            }
////
////        }
////    }
//}
