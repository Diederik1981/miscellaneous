package snippets.codility.stacksnqueues;

public class ProperNested2 {

    public static void main(String[] args) {
        System.out.println(solution("(()(())())"));
        System.out.println(solution("())"));
        System.out.println(solution("(()"));
        System.out.println(solution("(()())"));
    }


    public static int solution(String S) {
        int p = 0;

        int lastP = 0;

        for (int x = 0; x < S.length(); x++) {
            char tmp = S.charAt(x);
            switch (tmp) {

                case '(':
                    lastP = x;
                    p++;
                    break;

                case ')':
                    if (p <= 0 || lastP >= 0 && (x - lastP) % 2 == 0) {
                        return 0;
                    }
                    p--;
                    lastP = -1;
                    break;

            }
        }

        return p == 0 ? 1 : 0;
    }


}
