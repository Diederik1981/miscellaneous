package snippets.codility.stacksnqueues;

public class ProperNesting {

    public static void main(String[] args) {
//        System.out.println(solution("[]{}()"));
//        System.out.println(solution("([]{}())"));
//        System.out.println(solution("([)()]"));
        System.out.println(solution("{([[[()]]])}"));
        System.out.println(solution("{()}{"));


    }


    public static int solution(String S) {
        int p = 0;
        int b = 0;
        int br = 0;

        int lastP = 0;
        int lastB = 0;
        int lastBr = 0;

        for (int x = 0; x < S.length(); x++) {
            char tmp = S.charAt(x);
            switch (tmp) {
                case '{':
                    lastB = x;
                    b++;
                    break;

                case '}':
                    if (b <= 0 || lastB >= 0 && (x - lastB) % 2 == 0) {
                        return 0;
                    }
                    b--;
                    lastB = -1;
                    break;

                case '(':
                    lastP = x;
                    p++;
                    break;

                case ')':
                    if (p <= 0 || lastP >= 0 && (x - lastP) % 2 == 0) {
                        return 0;
                    }
                    p--;
                    lastP = -1;
                    break;

                case '[':
                    lastBr = x;
                    br++;
                    break;

                case ']':
                    System.out.println("------");
                    if (br <= 0 || lastBr >= 0 && (x - lastBr) % 2 == 0) {
                        return 0;
                    }
                    lastBr = -1;
                    br--;
                    break;
            }
        }

        return (p == 0 && b == 0 && br == 0) ? 1 : 0;
    }

    public static int solution2(String S) {
        boolean found = true;
        StringBuilder sb = new StringBuilder(S);
        while (found) {
            found = false;
            for (int x = 0; x < sb.length() - 1; x++) {
                char tmp = sb.charAt(x);
                char tmp2 = sb.charAt(x + 1);
                int count = 0;
                while (tmp == '(' && tmp2 == ')' || tmp == '{' && tmp2 == '}' || tmp == '[' && tmp2 == ']') {
                    count++;
                    if (x + count * 2 + 1 >= sb.length()) {
                        break;
                    }
                    tmp = sb.charAt(x + count * 2);
                    tmp2 = sb.charAt(x + count * 2 + 1);
                }
                if (count > 0) {
                    sb.delete(x, x + (2 * count));
                    found = true;
                    x -= 1;
                }
            }
        }
        return sb.length() == 0 ? 1 : 0;
    }


//    pu

}
