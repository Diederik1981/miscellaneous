package snippets.codility;

public class MinAvgSlice {

    public static void main(String[] args) {
//        System.out.println(solution(new int[]{4,4, 2, -7, 7}));
        System.out.println(solution(new int[]{-3, -5, -8, -4, -10}));

    }


    public static int solution(int[] A) {

        float min = Float.MAX_VALUE;
        int index = 0;
        for (int x = 0; x < A.length - 1; x++) {
            if (A[x] > min && A[x+1] > min) {
                x++;
            }

            float tmp = A[x];
            float div = 1;
            float fr = Float.MAX_VALUE;
            //System.out.println("----------------x" + x);

            for (int y = x + 1; y < A.length; y++) {
                div++;
                tmp += A[y];

                float tmpfr = tmp / div;

                if (tmpfr > fr) {
                    y = A.length;
                }
                if (fr < Float.MAX_VALUE) {
                    if (tmpfr > min) {
                        y = A.length;
                    }
                }
                fr = tmpfr;
                if (fr < min) {
                    min = fr;
                    index = x;
                }
//                System.out.println("tmp: " + tmp + " div: " + div + " fr: " + fr + " x: " + x);
            }
        }
        return index;
    }
}
