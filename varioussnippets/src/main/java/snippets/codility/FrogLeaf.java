package snippets.codility;

public class FrogLeaf {

    public static void main(String[] args) {
        System.out.println(solution(5, new int[]{1, 3, 1, 4, 2, 3, 5, 4}));
        System.out.println(solution(5, new int[]{1, 3, 1, 4}));
        System.out.println(solution(1, new int[]{1,1}));
    }

    public static int solution(int X, int[] A) {
        boolean[] positions = new boolean[X + 1];
        int countDown = (X * (X + 1)) / 2;

        for (int x = 0; x < A.length; x++) {
            int tmp = A[x];
            if (!positions[tmp]) {
                countDown -= tmp;
                if (countDown == 0) {
                    return x;
                }
                positions[tmp] = true;
            }

        }
        return -1;
    }
}
