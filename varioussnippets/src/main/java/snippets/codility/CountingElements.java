package snippets.codility;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CountingElements {


    public static void main(String[] args) {
        System.out.println(Arrays.toString(solution(5, new int[]{3, 4, 4, 1, 6, 6, 1, 4, 4})));
        System.out.println(Arrays.toString(solution(5, new int[]{3, 4, 4, 1, 1, 4, 4})));
        System.out.println(Arrays.toString(solution(5, new int[]{6,6,6})));
    }

    public static int[] solution(int N, int[] A) {
        Map<Integer, Integer> map = new HashMap<>();
        int max = 0;
        int globalBase = 0;
        for (int x : A) {
            if (x == N + 1) {
                globalBase += max;
                map = new HashMap<>();
                max = 0;
            } else {
                Integer tmp = map.get(x);
                if (tmp == null) {
                    map.put(x, 1);
                    max = Math.max(max, 1);
                } else {
                    tmp++;
                    max = Math.max(tmp, max);
                    map.put(x, tmp);
                }
            }
        }

        int[] result = new int[N];
        Arrays.fill(result, globalBase);
        for (Map.Entry<Integer, Integer> e : map.entrySet()) {
            result[e.getKey() - 1] += e.getValue();
        }

        return result;
    }


}
