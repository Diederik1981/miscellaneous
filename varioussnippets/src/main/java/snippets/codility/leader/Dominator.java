package snippets.codility.leader;

public class Dominator {

    public static void main(String[] args) {
        System.out.println(solution(new int[]{2, 1, 1, 1, 3}));
        System.out.println(solution(new int[]{2, 1, 1, 1, 3}));
    }

    public static int solution(int[] arr) {
        int m = 0;
        int i = 0;
        for (int x : arr) {
            if (i == 0) {
                m = x;
                i = 1;
            } else if (m == x) {
                i++;
            } else {
                i = i - 1;
            }
        }
        int count = 0;
        int index = 0;
        for (int x = 0; x < arr.length; x++) {
            if (arr[x] == m) {
                count++;
                index = x;
            }
        }
        return (count > arr.length / 2) ? index : -1;
    }
}
