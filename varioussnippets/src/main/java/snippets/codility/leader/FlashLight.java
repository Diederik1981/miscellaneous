package snippets.codility.leader;

public class FlashLight {

    public static void main(String[] args) {
        System.out.println(solution("U", 5, new int[]{-1, -2, 4, 1, 3, 0}, new int[]{5, 4, 3, 3, 1, -1}));
        System.out.println(solution("R", 5, new int[]{-2, 3}, new int[]{0, 1}));
    }

    private static final Double EPSILON = Double.MIN_VALUE * 10;

    public static int solution(String direction, int radius, int[] X, int[] Y) {

        int counter = 0;

        for (int i = 0; i < X.length; i++) {
            int x = X[i];
            int y = Y[i];
            switch (direction) {
                case "U":
                    if (y < 0) continue;
                    if (x < 0 && -x > y) continue;
                    if (x > 0 && x > y) continue;
                    break;
                case "D":
                    if (y > 0) continue;
                    if (x < 0 && -x > -y) continue;
                    if (x > 0 && x > -y) continue;
                    break;
                case "L":
                    if (x > 0) continue;
                    if (y < 0 && -y > -x) continue;
                    if (y > 0 && y > -x) continue;
                    break;
                case "R":
                    if (x < 0) continue;
                    if (y < 0 && -y > x) continue;
                    if (y > 0 && y > x) continue;
                    break;
            }
            if (!isWithinRadius(x, y, radius)) continue;
            counter++;
        }
        return counter;
    }

    private static boolean isWithinRadius(int x, int y, int radius) {
        return (double) radius >= Math.sqrt(Math.pow((double) x, 2) + Math.pow((double) y, 2)) - EPSILON;
    }
}
