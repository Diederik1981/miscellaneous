package snippets.codility;

public class FrogJump {

    public static void main(String[] args) {

        System.out.println(solution(10, 85, 30));
    }


    public static int solution(int X, int Y, int D) {

        int dist = Y - X;

        if (dist % D == 0) {
            return dist / D;
        }

        return dist / D + 1;
    }

}
