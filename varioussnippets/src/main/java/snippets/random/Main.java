package snippets.random;

public class Main {
    double[] mathScores = new double[4];

    {
        mathScores[0] = 94.5;
        mathScores[2] = 76.8;
    }


    public static void main(String[] args) {

        String[] students = {"Sade", "Alexus", "Sam", "Koma"};
        System.out.println(students[1]);
        Main main = new Main();
        System.out.println(main.mathScores[2]);
    }
}