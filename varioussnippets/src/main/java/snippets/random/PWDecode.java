package snippets.random;

/**
 * For an encode string where for A-Z the the replacement char is given in order.
 * Create a Decode string that does the same reversed.
 */
public class PWDecode {

    public static void main(String[] args) {
        String input = "QZERTYUIOPASDFGHJKLWXCVBNM";
        char[] output = new char[input.length()];

        for (int x = 0; x < input.length(); x++) {
            output[input.charAt(x) -'A'] = (char) (x +'A');
        }
        System.out.println(String.valueOf(output));
    }
}
