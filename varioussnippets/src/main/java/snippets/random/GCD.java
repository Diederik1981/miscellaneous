package snippets.random;

public class GCD {


    public static void main(String[] args) {
        System.out.println(gcd(10,100));
        System.out.println(gcd(5,100));
        System.out.println(gcd(21,144));
        System.out.println(gcd(21,144));
    }


    static int gcd(int a, int b) {
//        System.out.println("a:" + a + " b:" + b);
        if (b == 0) {
            return a;
        }
        return gcd(b, a % b);
    }


}
