package snippets.random;

public class Str {

    private char[] data;
    private transient String lazy;

    public Str(String lazy) {
        this.lazy = lazy;
    }

    public void Map() {
    }

    public char[] getData() {
        if (data == null) {
            return lazy.toCharArray();
        }
        return data;
    }

    public void setData(char[] data) {
        lazy = null;
        this.data = data;
    }

    @Override
    public String toString() {
        return data == null ? lazy : String.valueOf(data);
    }
}
