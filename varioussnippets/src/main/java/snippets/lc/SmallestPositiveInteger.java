package snippets.lc;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class SmallestPositiveInteger {
    public static void main(String[] args) {

        System.out.println(solution(new int[]{1, 3, 6, 4, 1, 2}));
        System.out.println(solution(new int[]{5, 3, 6, 4, 1, 2}));
        System.out.println(solution(new int[]{5, 3, 6, 1, 2}));
        System.out.println(solution(new int[]{-5, -3, -6, -4, -1, -2}));
        System.out.println(solution(new int[]{}));
        System.out.println(solution(null));

    }


    public static int solution(int[] A) {
        if (A == null) {
            return 1;
        }
        Arrays.sort(A);
        int index = 0;

        while (index < A.length && A[index] < 1) {
            index++;
        }
        int target = 1;
        while (index < A.length && A[index] <= target) {
            if (A[index] == target) {
                target++;
            }
            index++;
        }

        return target;
    }
//    public static int solution(int[] A) {
//        if (A == null) {
//            return 1;
//        }
//        Set<Integer> ints = new TreeSet<>();
//        for (int x : A) {
//            if (x > 0) {
//                ints.add(x);
//            }
//        }
//        int target = 1;
//        for (int x : ints) {
//            if (x != target) {
//                return target;
//            }
//            target++;
//        }
//        return target;
//    }
}
