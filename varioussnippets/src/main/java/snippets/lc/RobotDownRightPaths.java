package snippets.lc;

public class RobotDownRightPaths {

    public static void main(String[] args) {
        System.out.println(uniquePathsWithObstacles(new int[][]{
                {0, 0, 0}, {0, 0, 0}, {0, 0, 0}


        }));
    }

    public static int getTotalPaths(int[][] arr, int row, int col) {
        if (row >= arr.length || col >= arr[0].length || arr[row][col] == 1) {
            return 0;
        }
        if (row == arr.length - 1 && col == arr[0].length - 1) {
            return 1;
        }

        return getTotalPaths(arr, row + 1, col) + getTotalPaths(arr, row, col + 1);
    }

    public static int uniquePathsWithObstacles(int[][] obstacleGrid) {
        return getTotalPaths(obstacleGrid, 0, 0);
    }


}