package snippets.lc;

public class Primes {

    private static int[] primes = new int[5000010];

    static {
        int target = (int) Math.sqrt(primes.length);
//        System.out.println(target);
        for (int x = 2; x <= target; x++) {
            if (primes[x] > 0) continue;
            primes[x] = 1;
            for (int y = x + x; y < primes.length; y += x) {
                primes[y] = 2;
            }
        }
        int count = 0;
        for (int x = 2; x < primes.length; x++) {
//            System.out.println(count);
            if (primes[x] == 1 || primes[x] == 0) {
                primes[x] = count;
                count++;
            } else {
                primes[x] = count;
            }
        }
    }

    public static int countPrimes(int n) {
        return primes[n];
    }

    public static void main(String[] args) {
        System.out.println(countPrimes(0));
        System.out.println(countPrimes(1));
        System.out.println(countPrimes(2));
        System.out.println(countPrimes(3));
        System.out.println(countPrimes(10000));
    }
}
