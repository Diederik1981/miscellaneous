package snippets.coursera;

import java.util.Arrays;

/**
 * By clicking on squares, draw 16 diagonals that do not touch each other.
 */
public class BackslashPuzzle2 {

    public static int counter = 0;

    public static void main(String[] args) {
        int size = 6;
        checkMax(new int[0], 0, size);
        System.out.println("max: " + counter);
    }


    private static void checkMax(int[] arr, int count, int size) {
        if (arr.length >= size * size) {
//            if (count == 16) {
//                System.out.println("--");
//                for (int x = 0; x < arr.length; x++) {
//                    if (x % size == 0) System.out.println();
//
//                    if (arr[x] == 1) {
//                        System.out.print('\\');
//                    } else if (arr[x] == 2) {
//                        System.out.print('/');
//                    } else {
//                        System.out.print('0');
//                    }
//                }
//            }
////            System.out.println("temp: " + counter);
            counter = Math.max(count, counter);
            return;
        }

        int[] tmp = Arrays.copyOf(arr, arr.length + 1);
        tmp[tmp.length - 1] = 0;
        checkMax(tmp, count, size);

        tmp = Arrays.copyOf(arr, arr.length + 1);
        tmp[tmp.length - 1] = 1;
        if (checkSolution(tmp, size, count)) {
            checkMax(tmp, count + 1, size);
        }

        tmp = Arrays.copyOf(arr, arr.length + 1);
        tmp[tmp.length - 1] = 2;
        if (checkSolution(tmp, size, count)) {
            checkMax(tmp, count + 1, size);
        }

    }

    private static boolean checkSolution(int[] arr, int size, int count) {
        if ((count + size) < arr.length/2) {
            return false;
        }
        int len = arr.length;
        int tmp = arr[len - 1];
        int dist = len % size;

        if (dist != 1 && tmp + arr[len - 2] == 3) return false;
        if (len > size) {
            if (tmp + arr[len - 1 - size] == 3) return false;
            if (dist != 1 && tmp == 1 && arr[len - 2 - size] == 1) return false;
            if (dist < size && tmp == 2 && arr[len - 1 - size + 1] == 2) return false;
        }
        return true;
    }
}
