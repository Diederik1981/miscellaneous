package snippets.coursera;

/**
 * what is the biggest amount not payable with coins of 5 and 7
 * - biggest is 23
 */
public class LargestAmountNotPayableWith7or5 {


    public static void main(String[] args) {
        for (int x = 1; x < 10000; x++) {

            for (int y = 0; y < 100; y++) {
                int tmp = 7 * y;
                if (tmp > x) {
                    System.out.println(x);
                }
                if ((x - tmp) % 5 == 0) {
                    y = 1000;
                }
            }
        }
    }
}


