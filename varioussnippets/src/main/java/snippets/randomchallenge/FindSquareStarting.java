package snippets.randomchallenge;

/**
 * Question 1
 *
 * Find a 5-digit integer NNN such that N2N^2N2  starts with 27182.
 */
public class FindSquareStarting {

    public static void main(String[] args) {

        for (int x = 10000; x < 100000; x++) {
            int sq = x * x;
            String sqstr = Integer.toString(sq);
            if (sqstr.startsWith("27182")) {
                System.out.println(x + " " + sqstr);
            }
        }
    }
}
