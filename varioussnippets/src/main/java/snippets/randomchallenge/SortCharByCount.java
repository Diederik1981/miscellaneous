package snippets.randomchallenge;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;

class SortCharByCount {

    public static void main(String[] args) {

        System.out.println((int) 'a');

        String str = "asfsdf";
        for (char c : str.toCharArray()) {
            System.out.println((int) c - 'a');
        }


        System.out.println(order("aaaaaaaaaaaaaaasssssssuuu")

        );
    }


    public static String order(String input) {

        char[] count = new char[26];

        for (int x = 0; x < input.length(); x++) {
            count[input.charAt(x) - 'a']++;
        }

        List<Pair> pairs = new ArrayList<>();
        for (int x = 0; x < count.length; x++) {
            if (count[x] != 0) {
                pairs.add(new Pair(count[x], (char) (x + 'a')));
            }
        }
        return pairs.stream().sorted((a, b) -> Integer.compare(a.count, b.count))
                .map(p -> p.value)
                .collect(Collector.of(
                        StringBuilder::new,
                        StringBuilder::append,
                        StringBuilder::append,
                        StringBuilder::toString));

    }

    private static class Pair {
        char value;
        int count;

        Pair(int c, char v) {
            this.value = v;
            this.count = c;
        }
    }


}